# Example Usage
# bash <(curl -s https://git.grassecon.net/grassrootseconomics/cic-staff-installer/raw/branch/lum/kitabu/uninstall.sh)
# This is currently living on at pip because root is using a self signed cert
# When changes are made to this file you must run bash ./rsync/rsync-install-script.sh to sync it to the server

red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`


echo -n "
${red}WARNING:${reset} The following will be DELETED if present:
  Directories:
    $HOME/.local/share/cic/cache
    $HOME/.local/share/cic/clicada
    $HOME/.config/cic/clicada
    $HOME/.config/cic/cache
  Files:
    $HOME/.config/cic/staff-client/.envinit
    $HOME/.config/systemd/user/cic-cache-tracker.service
    $HOME/.config/systemd/user/cic-cache-server.service
    $HOME/.config/environment.d/01-cic-cache-server.conf
    $HOME/.config/environment.d/01-cic-cache-tracker.conf
    $HOME/.local/bin/cic_cache_server_start.sh
    $HOME/.local/bin/migrate_cic_cache.py

Would you still like to continue? (y/n):"
read confirmed

if [ "$confirmed" == "y" ]; then
    echo "Stopping Services.."
    systemctl --user stop cic-cache-tracker
    systemctl --user stop cic-cache-server
    systemctl --user disable cic-cache-tracker
    systemctl --user disable cic-cache-server
    echo "Deleting..."
    pip uninstall --user clicada cic cic-cache 
    rm -rf $HOME/.local/share/cic/cache
    rm -rf $HOME/.local/share/cic/clicada
    
    rm -rf $HOME/.config/cic/clicada
    rm -rf $HOME/.config/cic/cache
    rm $HOME/.config/cic/staff-client/.envinit
    rm $HOME/.config/systemd/user/cic-cache-tracker.service
    rm $HOME/.config/systemd/user/cic-cache-server.service
    
    rm $HOME/.config/environment.d/01-cic-cache-server.conf
    rm $HOME/.config/environment.d/01-cic-cache-tracker.conf
    rm $HOME/.local/bin/cic_cache_server_start.sh
    rm $HOME/.local/bin/migrate_cic_cache.py
    echo "${green}Done${reset}"
else
    echo "Aborting"
fi

echo -n "${red}WARNING:${reset}  The following will be DELETED if present:
  Directories:
    $HOME/.local/share/cic/.gnupg
    $HOME/.config/cic/staff-client/.gnupg
  Files:
    $HOME/.config/cic/staff-client/user.asc
    $HOME/.config/cic/staff-client/key_fingerprint
Would you also like to delete these cic keyrings? (y/n):
"
read confirmed
if [ "$confirmed" == "y" ]; then
    echo "Deleting all keychains..."
    rm -rf $HOME/.local/share/cic/.gnupg
    rm -rf $HOME/.config/cic/staff-client/.gnupg
    echo "${green}Done${reset}"
else
    echo "Aborting"
fi
exit 0
