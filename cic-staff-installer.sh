# Example Usage
# bash <(curl -s https://git.grassecon.net/grassrootseconomics/cic-staff-installer/raw/branch/lum/kitabu/install.sh) -h  
# This is currently living on at pip because root is using a self signed cert
# When changes are made to this file you must run bash ./rsync/rsync-install-script.sh to sync it to the server

branch='master'
verbose='false'
evm= # kitabu | bloxberg 

print_usage() {
  printf "CIC Staff Installer
  Options: 
    -b <branch>     Name of the branch you want use to install
    -e <evm>        Name of the EVM you want to install (kitabu, bloxberg; default is none)
    -v              Verbose
    -h              Print this help message
"
}

while getopts 'e:hb:v' flag; do
  case "${flag}" in
    b) branch="${OPTARG}" ;;
    e) evm="${OPTARG}" ;;
    v) verbose='true' ;;
    h) print_usage 
       exit 0 ;;
    *) print_usage
       exit 1 ;;
  esac
done
export INSTALL_EVM=${evm}
if [ ! -z $INSTALL_EVM ]; then
    . setup_evm.sh
fi
# temp_dir=`mktemp -d`
# git clone -b $branch https://git.grassecon.net/grassrootseconomics/cic-staff-installer.git $temp_dir
# cd $temp_dir

export BASH_DEBUG_LEVEL=4 # all=1 trace=2 debug=3 info=4 warn=5 error=6
export BASH_DEBUG=1  
bash setup.sh